#include <stdint.h> 
#include <stdlib.h> 
#include <math.h>
#include "stm32f4xx_hal.h" 
#include "cmsis_os2.h"
#include "uart.h" 
#include "pendulum.h" 
# include "data_logging.h"

/* Variable declarations */
double logCount;

static osTimerId_t _dataLogTimerID;
static osTimerAttr_t _dataLogTimerAttr = {
	.name = "datalogging"
}; 

/* Function declarations */
static void log_pendulum(void *argument);

/* Function defintions */
static void log_pendulum(void *argument)
{
    /* TODO: Supress compiler warnings for unused arguments */
    UNUSED(argument);
    /* TODO: Read the potentiometer voltage */
    float V_adc = pendulum_read_voltage();
    /* TODO: Print the sample time and potentiometer voltage to the serial terminal in the format [time],[
    voltage] */
    printf("%lf, %f\n", logCount, V_adc);
    /* TODO: Increment log count */
    logCount=logCount+0.005;
    /* TODO: Stop logging once 2 seconds is reached (Complete this once you have created the stop function
    in the next step) */
    if (logCount>=2){
	    pend_logging_stop();
	}
}

void logging_init(void)
{
    /* TODO: Initialise timer for use with pendulum data logging */
    _dataLogTimerID = osTimerNew(log_pendulum, osTimerPeriodic, NULL, &_dataLogTimerAttr);
}

void pend_logging_start(void)
{
    /* TODO: Reset the log counter */
    logCount = 0;
    /* TODO: Start data logging timer at 200Hz */
    osTimerStart(_dataLogTimerID, 5); 
}

void pend_logging_stop(void)
{
    /* TODO: Stop data logging timer */
    osTimerStop(_dataLogTimerID); 
}

